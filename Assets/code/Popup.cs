using Doozy.Runtime.UIManager.Containers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace _popup
{
    public class Popup : MonoBehaviour
    {
        public string PopupName = "Mypopup";

        [Header("Popup")]
        public string PopupTitle = "mypopup";
        public string PopupMessage = "This is a popup message";

        [Header("Button")]
        public string PopupButtonText = "OK";
        public Sprite PopupButtonIcon;
        public UnityEvent OnPopupButtonClicked = new UnityEvent();

        public void ShowPopup()
        {
            var popup = UIPopup.Get(PopupName);
            if (popup == null) return;
            popup
                .SetTexts(PopupTitle, PopupMessage, PopupButtonText)
                .SetSprites(PopupButtonIcon)
                .SetEvents(OnPopupButtonClicked)
                .Show();
        }

        public void DebugTest(string message)
        {
            Debug.Log(message);
        }

    }
}